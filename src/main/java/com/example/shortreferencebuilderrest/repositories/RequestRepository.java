package com.example.shortreferencebuilderrest.repositories;

import com.example.shortreferencebuilderrest.models.RequestInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Date: 13.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * Request's JPA repository.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Repository
public interface RequestRepository extends JpaRepository<RequestInfo, Long> {
    List<RequestInfo> findByReferenceIdOrderByIdAsc(Long referenceId);
}

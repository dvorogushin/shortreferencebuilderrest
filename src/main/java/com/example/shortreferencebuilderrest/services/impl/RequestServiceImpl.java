package com.example.shortreferencebuilderrest.services.impl;

import com.example.shortreferencebuilderrest.exceptions.RequestNotFoundException;
import com.example.shortreferencebuilderrest.models.Reference;
import com.example.shortreferencebuilderrest.models.RequestInfo;
import com.example.shortreferencebuilderrest.repositories.RequestRepository;
import com.example.shortreferencebuilderrest.services.ReferenceService;
import com.example.shortreferencebuilderrest.services.RequestService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Date: 13.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * Service implementation class for operations with {@code requests}.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Service
@RequiredArgsConstructor
public class RequestServiceImpl implements RequestService {

    /**
     * Request repository object.
     */
    public final RequestRepository requestRepository;

    /**
     * Finds and returns all requests to the {@code reference}.
     *
     * @param referenceId ID of the {@code reference}.
     * @return Collection of {@link RequestInfo} objects.
     */
    @Override
    public List<RequestInfo> getRequests(Long referenceId) {
       return requestRepository.findByReferenceIdOrderByIdAsc(referenceId);
    }

    /**
     * Finds a {@code request} by ID. Used to get {@code Cookies}.
     *
     * @param requestId ID of the {@code request}.
     * @return {@link RequestInfo} object.
     * @throws RequestNotFoundException if ID is absent.
     */
    @Override
    public RequestInfo getRequest(Long requestId) {
        RequestInfo requestInfo = requestRepository
                .findById(requestId)
                .orElseThrow(RequestNotFoundException::new);
        return requestInfo;
    }
}


//    // использовалась при пагинации когда данные брались сразу из БД для RequestInfo
//    @Override
//    public Page<RequestInfo> getRequests(Long referenceId, Pageable pageRequest) {
//
//        // запрос к базе через фильтр по referenceId (JPA) - тоже работает
////        Page<RequestInfo> pagedRequests = requestRepository.findAllByReferenceId(referenceId, pageRequest);
//
//        // получение списка запросов (List<RequestInfo>) через обращение в модель Reference (Reference.gerRequests())
//        List<RequestInfo> requestsList = referenceService.getReference(referenceId).getRequests();
//
//        // пагинация полученного списка путем выделения подсписка
//        final int start = (int) pageRequest.getOffset();
//        final int end = Math.min((start + pageRequest.getPageSize()), requestsList.size());
//        final Page<RequestInfo> pagedRequests = new PageImpl<>(
//                requestsList.subList(start, end),
//                pageRequest,
//                requestsList.size());
//
//        return pagedRequests;
//    }


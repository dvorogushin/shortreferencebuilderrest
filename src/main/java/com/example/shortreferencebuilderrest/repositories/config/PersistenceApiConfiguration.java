package com.example.shortreferencebuilderrest.repositories.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityGraph;
import javax.sql.DataSource;
import java.util.HashMap;

/**
 * Date: 28.04.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p> Configurer of the persistence db - PostgreSQL. <br>
 * Defines entity classes, driver settings and other.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Configuration
@EnableJpaRepositories(
        basePackages = "com.example.shortreferencebuilderrest.repositories",
        entityManagerFactoryRef = "apiEntityManager",
        transactionManagerRef = "apiTransactionManager")
public class PersistenceApiConfiguration {

    @Autowired
    private Environment env;

    /**
     * Defines and returns entity manager.
     * @return
     */
    @Bean
    public LocalContainerEntityManagerFactoryBean apiEntityManager() {

        LocalContainerEntityManagerFactoryBean em
                = new LocalContainerEntityManagerFactoryBean();

        em.setDataSource(apiDataSource());
        // entity's package
        em.setPackagesToScan(new String[] { "com.example.shortreferencebuilderrest.models" });

        HibernateJpaVendorAdapter vendorAdapter
                = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto",
                env.getProperty("hibernate.hbm2ddl.auto"));
        properties.put("hibernate.dialect",
                env.getProperty("hibernate.dialect.postgresql"));
        em.setJpaPropertyMap(properties);
        return em;
    }

    /**
     * Reads db properties.
     *
     * @return
     */
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource apiDataSource() {
        return DataSourceBuilder.create().build();
    }

    /**
     * Defines transaction manager.
     * @return
     */
    @Bean
    public PlatformTransactionManager apiTransactionManager() {
        JpaTransactionManager transactionManager
                = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(
                apiEntityManager().getObject());
        return transactionManager;
    }
}
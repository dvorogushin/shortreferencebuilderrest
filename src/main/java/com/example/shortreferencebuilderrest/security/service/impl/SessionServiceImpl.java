package com.example.shortreferencebuilderrest.security.service.impl;

import com.example.shortreferencebuilderrest.exceptions.BadTokenException;
import com.example.shortreferencebuilderrest.models.User;
import com.example.shortreferencebuilderrest.security.model.Token;
import com.example.shortreferencebuilderrest.security.model.Session;
import com.example.shortreferencebuilderrest.security.repository.SessionRepository;
import com.example.shortreferencebuilderrest.security.service.SessionService;
import lombok.RequiredArgsConstructor;
import org.jose4j.jwx.Headers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.*;

/**
 * Date: 29.04.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p> Service layer for {@code session} entity.
 *
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Service
@RequiredArgsConstructor
public class SessionServiceImpl implements SessionService {

    public static final String HEADER_NAME_KID = "kid";

    @Autowired
    private SessionRepository sessionRepository;

    /**
     * Creates client's session.
     * Creates and returns the access and refresh tokens.
     *
     * @param user
     * @return
     */
    @Override
    public Set<Token> createSession(User user) {

        Set<Token> tokens = new HashSet<>();

        // create and save client's session
        Session session = new Session();
        sessionRepository.save(session);

        // create access token
        Token accessToken = Token.builder()
                .session(session)
                .user(user)
                .type(Token.Type.ACCESS)
                .build();
        accessToken.encrypt();
        tokens.add(accessToken);

        // create refresh tokens
        Token refreshToken = Token.builder()
                .session(session)
                .user(user)
                .type(Token.Type.REFRESH)
                .build();
        refreshToken.encrypt();
        tokens.add(refreshToken);

        return tokens;
    }

    /**
     * Finds and returns the session by ID.
     * @param sessionId
     * @return
     */
    @Override
    public Session getSession(Long sessionId) {
        return sessionRepository
                .findById(sessionId)
                .orElseThrow(BadTokenException::new);
    }

    /**
     * Get all sessions.
     * @return
     */
    @Override
    public List<Session> getAll() {
        return sessionRepository.findAll();
    }

    /**
     * Obtains a valid refresh token. Updates the keys of the session.
     * Creates and returns new access and refresh tokens.
     *
     * @param token
     * @return
     */
    @Override
    public Set<Token> refreshTokens(Token token) {
        Set<Token> tokens = new HashSet<>();
        Session session = token.getSession();
        // create keys
        session.setEncValue(UUID.randomUUID());
        session.setSigValue(UUID.randomUUID());
        sessionRepository.save(session);
        // create access token
        Token accessToken = Token.builder()
                .session(session)
                .user(token.getUser())
                .type(Token.Type.ACCESS)
                .build();
        accessToken.encrypt();
        tokens.add(accessToken);
        // create refresh tokens
        Token refreshToken = Token.builder()
                .session(session)
                .user(token.getUser())
                .type(Token.Type.REFRESH)
                .build();
        refreshToken.encrypt();
        tokens.add(refreshToken);

        return tokens;
    }

    /**
     * Decrypts and validates the token.
     * @param encryptedToken
     * @return
     */
    @Override
    public Token getDecryptedToken(String encryptedToken) {

        // decode header to read token_id ("kid")
        String[] chunks = encryptedToken.split("\\.");
        String header = new String((Base64.getUrlDecoder().decode(chunks[0])));
        Long tokenId = null;
        try {
            Headers headers = new Headers();
            headers.setFullHeaderAsJsonString(header);
            tokenId = Long.valueOf(headers.getStringHeaderValue(HEADER_NAME_KID));
            Assert.notNull(tokenId);

        } catch (Exception e) {
            throw new BadTokenException(e.getMessage());
        }

        // Get session from in-memory db
        Session tokenSession = getSession(tokenId);
        Token token = Token.builder()
                .session(tokenSession)
                .encryptedValue(encryptedToken)
                .build();
        token.decrypt();

        return token;

    }
}
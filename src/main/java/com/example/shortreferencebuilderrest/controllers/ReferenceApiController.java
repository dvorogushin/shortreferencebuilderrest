package com.example.shortreferencebuilderrest.controllers;

import com.example.shortreferencebuilderrest.compare.ObjectComparator;
import com.example.shortreferencebuilderrest.controllers.modelAssembler.ReferenceDtoModelAssembler;
import com.example.shortreferencebuilderrest.forms.ReferenceForm;
import com.example.shortreferencebuilderrest.services.ReferenceService;
import com.example.shortreferencebuilderrest.transfer.ReferenceDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static com.example.shortreferencebuilderrest.transfer.ReferenceDto.from;


/**
 * Date: 13.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p>Main controller. Realizes all CRUD operations.
 * As per request's header {@code Accept} returns data in {@code json} or {@code xml} format.
 * </p>
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Slf4j
@RestController
@RequiredArgsConstructor
public class ReferenceApiController {

    @Autowired
    private final ReferenceService referenceService;

    // для гиперссылок пагинации
    private final PagedResourcesAssembler pagedResourcesAssembler;

    @Autowired
    private final ReferenceDtoModelAssembler modelAssembler;

    /**
     * GET all
     * <p>Returns all references, wrapped in to the pageable part.
     * Hyperlinks to self are used.</p>
     *
     * @param pageable page number, page size in items, sort property, direction. Not required.
     * @return List of {@link ReferenceDto} objects.
     */
    @GetMapping(value = "/references")
    public ResponseEntity getAllReferences(
            @PageableDefault(page = 0, size = 2, sort = "id", direction = Sort.Direction.ASC)
                    Pageable pageable) {

        // формирование списка ссылок в формате Dto,
        // для применения сортировки по полям ReferenceDto
        List<ReferenceDto> referenceDtoList = from(referenceService.getAll());
        // multysorting collection as per PageRequest
        referenceDtoList.sort(new ObjectComparator<>(pageable, ReferenceDto.class));

        // пагинация полученного списка путем выделения подсписка (без сортировки!!!)
        int start = (int) pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), referenceDtoList.size());
        // если параметры выходят за область страницы
        if (start > end) {
            start = 0;
            end = 0;
        }
        final Page<ReferenceDto> pagedReferenceDto = new PageImpl<>(
                referenceDtoList.subList(start, end),
                pageable,
                referenceDtoList.size());

        // присоединение гиперссылок каталога и пр. (в соответствии с ReferenceDtoModelAssembler)
        List<EntityModel<ReferenceDto>> referencesEntity =
                // из Reference в ReferenceDto
                pagedReferenceDto.getContent()
                        .stream()
                        .map(modelAssembler::toModel)
                        .collect(Collectors.toList());


        // формирование страницы с данными о ней
        Page<EntityModel<ReferenceDto>> pagedEntity =
                new PageImpl<>(referencesEntity, pageable, pagedReferenceDto.getTotalElements());

        // формирование гиперссылок страницы (first/last/prev/next/self)
        return ResponseEntity
                .ok()
                .body(pagedResourcesAssembler.toModel(pagedEntity));
    }

// вариант гиперссылок без пагинации

//    @GetMapping("/references")
//    public CollectionModel<EntityModel<ReferenceDto>> getAll() {
//        List<EntityModel<ReferenceDto>> references =
//                from(referenceService.getAll()).stream()
//                        .map(assembler::toModel)
//                        .collect(Collectors.toList());
//        return CollectionModel.of(references,
//                linkTo(methodOn(ReferenceApiController.class)
//                        .getAll())
//                        .withSelfRel());
//    }

    /**
     * GET one
     * <p>Returns data for the one reference.
     * Hyperlinks to self are present.</p>
     *
     * @param referenceId ID of the reference.
     * @return {@link ReferenceDto} object.
     */
    @GetMapping(value = "/references/{reference-id}")
    public EntityModel<ReferenceDto> getReferenceById(@PathVariable("reference-id") Long referenceId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        authentication.getPrincipal().toString();
        return modelAssembler.toModel(from(referenceService.getReference(referenceId)));
    }

    // изменение состояния ссылки

    /**
     * PUT
     * <p>Changes a state of the reference between {@code ACTIVE} and {@code NOT_ACTIVE} states.
     * Returns the changed reference. Hyperlinks to self are present.</p>
     *
     * @param referenceId ID of the reference
     * @return {@link ReferenceDto} object.
     */
    @PutMapping("/references/{reference-id}")
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public EntityModel<ReferenceDto> changeState(@PathVariable("reference-id") Long referenceId) {
        return modelAssembler.toModel(from(referenceService.changeState(referenceId)));
    }

    /**
     * POST
     * <p>Creates and returns a new reference.
     * Hyperlinks to self are present.</p>
     *
     * @param form redirect URL and free period, wrapped in {@link ReferenceForm} object.
     * @return {@link ReferenceDto} object.
     */
    @PostMapping("/references")
    @ResponseStatus(HttpStatus.CREATED)
    public EntityModel<ReferenceDto> createReference(@RequestBody ReferenceForm form) {
        return modelAssembler.toModel(from(referenceService.createReference(form)));
    }

    /**
     * DELETE
     * <p>Deletes a reference.</p>
     *
     * @param referenceId ID of the reference
     * @return code 204 ({@code No_Content})
     */
    @DeleteMapping("/references/{reference-id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public ResponseEntity<?> deleteReference(@PathVariable("reference-id") Long referenceId) {
        referenceService.deleteReference(referenceId);
        return ResponseEntity.noContent().build();
    }
}


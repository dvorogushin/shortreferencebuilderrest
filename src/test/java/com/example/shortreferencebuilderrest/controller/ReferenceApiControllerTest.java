package com.example.shortreferencebuilderrest.controller;

import com.example.shortreferencebuilderrest.forms.ReferenceForm;
import com.example.shortreferencebuilderrest.models.Reference;
import com.example.shortreferencebuilderrest.services.ReferenceService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;


import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Date: 31.05.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p>
 * Tests {@link com.example.shortreferencebuilderrest.controllers.ReferenceApiController Reference} controller class. <br>
 * Service layer is mocked.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DisplayName("ReferenceApiController is being tested.")
@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)   // for @BeforeAll
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ReferenceApiControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReferenceService referenceService;

    List<Reference> referenceList;
    Reference testReference;

    /**
     * Creates 2 references and a collection of them, for all tests.
     */
    @BeforeAll
    public void setUp() {
        Reference reference1 = Reference.builder()
                .id(1L)
                .state(Reference.State.ACTIVE)
                .shortRef("test1")
                .redirectRef("www.google.com")
                .creationDateTime(LocalDateTime.now())
                .expirationDateTime(LocalDateTime.now())
                .build();

        Reference reference2 = Reference.builder()
                .id(2L)
                .state(Reference.State.ACTIVE)
                .shortRef("test2")
                .redirectRef("www.mail.ru")
                .creationDateTime(LocalDateTime.now())
                .expirationDateTime(LocalDateTime.now())
                .build();

        this.testReference = reference1;
        this.referenceList = Arrays.asList(reference1, reference2);
    }

    /**
     * Tests GetAll method.
     */
    @Nested
    @DisplayName("GetAllReferences() method is being tested.")
    class GetAllReferencesMethodTest {

        /**
         * Tests GetAll method. 'Accept' header has an 'application/json' string.
         * @throws Exception
         */
        @Test
        public void whenAcceptHeaderJSON_thanResponseJSON() throws Exception {

            given(referenceService.getAll()).willReturn(referenceList);

            mockMvc.perform(get("/references")
                            .accept(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$").isNotEmpty())
                    .andExpect(jsonPath("_embedded").exists())
                    .andExpect(jsonPath("$._embedded.entityModelList").exists())
                    .andExpect(jsonPath("$._embedded.entityModelList").isArray())
                    .andExpect(jsonPath("$._embedded.entityModelList", hasSize(2)))
                    .andExpect(jsonPath("$._embedded.entityModelList[0].id").value(1))
                    .andExpect(jsonPath("$._embedded.entityModelList[0]._links.requests").exists())
                    .andExpect(jsonPath("$._embedded.entityModelList[0]._links.self").exists())
                    .andExpect(jsonPath("$._embedded.entityModelList[0]._links.references").exists())
                    .andExpect(jsonPath("$._embedded.entityModelList[1].id").value(2))
                    .andExpect(jsonPath("$._embedded").isMap())
                    .andExpect(jsonPath("$._embedded[*]", hasSize(1)))
                    .andExpect((jsonPath("$._links").exists()))
                    .andExpect((jsonPath("$._links.self").exists()))
                    .andExpect((jsonPath("$.page").exists()));
        }

        /**
         * Tests GetAll method. 'Accept' header has an 'application/xml' string.
         * @throws Exception
         */
        @Test
        public void whenAcceptHeaderXML_thanResponseXML() throws Exception {

            given(referenceService.getAll()).willReturn(referenceList);

            mockMvc.perform(get("/references")
                            .accept(MediaType.APPLICATION_XML))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_XML))
                    .andExpect(xpath("PagedModel/links/rel", hasValue("self")).exists())
                    .andExpect(xpath("PagedModel/links/href", notNullValue()).exists())
                    .andExpect(xpath("PagedModel/content/content/id").nodeCount(2))
                    .andExpect(xpath("PagedModel/content/content/links[1]/rel", hasValue("requests")).exists())
                    .andExpect(xpath("PagedModel/content/content/links[2]/rel", hasValue("self")).exists())
                    .andExpect(xpath("PagedModel/content/content/links[3]/rel", hasValue("references")).exists())
                    .andExpect(xpath("PagedModel/content/content/links[1]/href", notNullValue()).exists())
                    .andExpect(xpath("PagedModel/content/content/links[2]/href", notNullValue()).exists())
                    .andExpect(xpath("PagedModel/content/content/links[3]/href", notNullValue()).exists())
                    .andExpect(xpath("PagedModel/page/size", notNullValue()).exists());
        }
    }

    /**
     * Tests GetOne method.
     */
    @Nested
    @DisplayName("GetReferenceById() method is being tested.")
    @WithMockUser(username="admin",roles={"USER","ADMIN"})
    class GetReferenceByIdMethodTest {

        /**
         * Tests GetOne method. 'Accept' header has an 'application/json' string.
         * @throws Exception
         */
        @Test
        public void whenAcceptHeaderJSON_thenResponseJSON() throws Exception {
            given(referenceService.getReference(ArgumentMatchers.any())).willReturn(testReference);

            mockMvc.perform(get("/references/{referenceId}", "1")
                            .accept(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$").isNotEmpty())
                    .andExpect(jsonPath("$.id").value(1))
                    .andExpect(jsonPath("$._links.requests").exists())
                    .andExpect(jsonPath("$._links.self").exists())
                    .andExpect(jsonPath("$._links.references").exists());
        }

        /**
         * Tests GetOne method. 'Accept' header has an 'application/xml' string.
         * @throws Exception
         */
        @Test
        public void whenAcceptHeaderXML_thanResponseXML() throws Exception {

            given(referenceService.getReference(1L)).willReturn(testReference);

            mockMvc.perform(get("/references/{referenceId}", "1")
                            .accept(MediaType.APPLICATION_XML))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_XML))
                    .andExpect(xpath("EntityModel/id", hasValue("1")).exists())
                    .andExpect(xpath("EntityModel/links[1]/rel", hasValue("requests")).exists())
                    .andExpect(xpath("EntityModel/links[2]/rel", hasValue("self")).exists())
                    .andExpect(xpath("EntityModel/links[3]/rel", hasValue("references")).exists())
                    .andExpect(xpath("EntityModel/links[1]/href", notNullValue()).exists())
                    .andExpect(xpath("EntityModel/links[2]/href", notNullValue()).exists())
                    .andExpect(xpath("EntityModel/links[3]/href", notNullValue()).exists());
        }
    }

    /**
     * Tests Put method.
     */
    @Nested
    @DisplayName("ChangeState() method is being tested.")
    class ChangeStateMethodTest {

        /**
         * Tests Put method. 'Accept' header has an 'application/json' string.
         * @throws Exception
         */
        @Test
        public void whenAcceptHeaderJSON_thenResponseJSON() throws Exception {

            given(referenceService.changeState(1L)).willReturn(testReference);

            mockMvc.perform(put("/references/{referenceId}", "1")
                            .accept(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().is(202))
                    .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$").isNotEmpty())
                    .andExpect(jsonPath("$.state").value(Reference.State.ACTIVE.toString()));
        }

        /**
         * Tests Put method. 'Accept' header has an 'application/xml' string.
         * @throws Exception
         */
        @Test
        public void whenAcceptHeaderXML_thenResponseXML() throws Exception {

            given(referenceService.changeState(1L)).willReturn(testReference);

            mockMvc.perform(put("/references/{referenceId}", "1")
                            .accept(MediaType.APPLICATION_XML))
                    .andDo(print())
                    .andExpect(status().is(202))
                    .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_XML))
                    .andExpect(xpath("/EntityModel/state", hasValue(Reference.State.ACTIVE.toString())).exists());
        }
    }

    /**
     * Tests Post method.
     */
    @Nested
    @DisplayName("createReference() method is being tested.")
    class CreateReferenceMethodTest {

        /**
         * Tests Post method. 'Accept' header has an 'application/json' string.
         * @throws Exception
         */
        @Test
        public void whenAcceptHeaderJSON_thenResponseJSON() throws Exception {

            when(referenceService.createReference(ArgumentMatchers.any(ReferenceForm.class)))
                    .thenReturn(testReference);

            mockMvc.perform(post("/references")
                            .content("{\"url\": \"www.google.com\",\"freePeriod\": 5}")
                            .accept(MediaType.APPLICATION_JSON)
                            .contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().is(201))
                    .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$").isNotEmpty())
                    .andExpect(jsonPath("$.state").value(Reference.State.ACTIVE.toString()));
        }


        /**
         * Tests Post method. 'Accept' header has an 'application/xml' string.
         * @throws Exception
         */
        @Test
        public void whenAcceptHeaderXML_thenResponseXML() throws Exception {

            when(referenceService.createReference(ArgumentMatchers.any((ReferenceForm.class))))
                    .thenReturn(testReference);

            mockMvc.perform(post("/references")
                            .content("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                                    "<ReferenceForm>\n" +
                                    "\t<url>www.google.com</url>\n" +
                                    "\t<freePeriod>5</freePeriod>\n" +
                                    "</ReferenceForm>")
                            .accept(MediaType.APPLICATION_XML)
                            .contentType(MediaType.APPLICATION_XML))
                    .andDo(print())
                    .andExpect(status().is(201))
                    .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_XML))
                    .andExpect(xpath("/EntityModel/state", hasValue(Reference.State.ACTIVE.toString())).exists());
        }

    }

    /**
     * Tests Delete method.
     */
    @Nested
    @DisplayName("deleteMethod() method is being tested.")
    class DeleteReferenceMethodTest {

        /**
         * Tests Delete method.
         * @throws Exception
         */
        @Test
        public void whenRequestDelete_thenResponseNo_Content() throws Exception {

            mockMvc.perform(delete("/references/{referenceId}", "1"))
                    .andDo(print())
                    .andExpect(status().is(204))
                    .andExpect(content().string(""))
                    .andExpect(content().bytes(new byte[0]));
        }

    }
}
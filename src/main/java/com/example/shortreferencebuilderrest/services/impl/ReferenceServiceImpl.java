package com.example.shortreferencebuilderrest.services.impl;

import com.example.shortreferencebuilderrest.exceptions.ReferenceNotFoundException;
import com.example.shortreferencebuilderrest.forms.ReferenceForm;
import com.example.shortreferencebuilderrest.models.Reference;
import com.example.shortreferencebuilderrest.repositories.ReferenceRepository;
import com.example.shortreferencebuilderrest.services.ReferenceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static com.example.shortreferencebuilderrest.models.Reference.*;

/**
 * Date: 13.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * Service implementation class for operations with the {@code references} entity.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ReferenceServiceImpl implements ReferenceService {

    /**
     * {@code Reference} repository object.
     */
    public final ReferenceRepository referenceRepository;

    /**
     * Finds the {@code reference} by ID.
     * Checks expiration date.
     *
     * @param referenceId ID of desired {@code reference}.
     * @return {@link Reference} object.
     * @throws ReferenceNotFoundException if {@code reference} ID is absent.
     */
    @Override
    public Reference getReference(Long referenceId) {
        log.info("getReference() real method had been invoked.");
        Reference reference = referenceRepository
                .findByIdOrderByIdAsc(referenceId)
                .orElseThrow(ReferenceNotFoundException::new);
        return lazyExpirationControl(reference);
    }

    /**
     * Finds and returns all references from {@link ReferenceServiceImpl#referenceRepository}.
     * All founded references are checked for an expiration in {@link ReferenceServiceImpl#lazyExpirationControl} method.
     *
     * @return Collection of {@link Reference} objects.
     */
    @Override
    public List<Reference> getAll() {
        List<Reference> references = referenceRepository.findByOrderByIdAsc();
        references.forEach(this::lazyExpirationControl);
        return references;
    }

    /**
     * Deletes the {@code reference}.
     *
     * @param referenceId ID of the desired reference.
     */
    @Override
    public void deleteReference(Long referenceId) {
        Reference reference = getReference(referenceId);
        referenceRepository.delete(reference);
    }

    /**
     * Changes the status of the {@code reference} between {@code ACTIVE} and {@code NOT_ACTIVE} states.
     *
     * @param referenceId ID of the desired {@code reference}.
     * @return {@link Reference} object.
     */
    @Override
    public Reference changeState(Long referenceId) {
        Reference reference = getReference(referenceId);
        if (reference.getState().equals(Reference.State.NOT_ACTIVE)
                && reference.getExpirationDateTime().isAfter(LocalDateTime.now())) {
            reference.setState(Reference.State.ACTIVE);
        } else {
            reference.setState(Reference.State.NOT_ACTIVE);
        }
        referenceRepository.save(reference);
        return reference;
    }

    /**
     * Creates new {@code reference}.
     *
     * @param form {@link ReferenceForm} object (URL and {@code freePeriod}).
     * @return {@link Reference} object.
     */
    @Override
    public Reference createReference(ReferenceForm form) {
        Reference reference = Reference.builder()
                .creationDateTime(LocalDateTime.now())
                .expirationDateTime(LocalDateTime.now().plusDays(form.getFreePeriod()))
                .redirectRef(form.getUrl())
                .shortRef(Reference.create(ReferenceCreator::createShortReference))
                .state(Reference.State.ACTIVE)
                .build();
        referenceRepository.save(reference);
        return reference;
    }

    /**
     * Checks expiration date of the {@code reference}.
     * If expiration date has passed than state is changed to a {@code NOT_ACTIVE} value and owner became {@code null}.
     *
     * @param reference {@link Reference} object.
     * @return The same {@link Reference} object.
     */
    public Reference lazyExpirationControl(Reference reference) {
        if (Reference.validate(ReferenceValidator::isExpired, reference)) {
            if (!reference.getState().equals(State.NOT_ACTIVE) || reference.getOwnerUser() != null) {
                reference.setState(Reference.State.NOT_ACTIVE);
                reference.setOwnerUser(null);
                referenceRepository.save(reference);
            }
        }
        return reference;
    }
}

package com.example.shortreferencebuilderrest.security.repository;

import com.example.shortreferencebuilderrest.security.model.Session;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Date: 28.04.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p>
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
public interface SessionRepository extends JpaRepository<Session, Long> {


}
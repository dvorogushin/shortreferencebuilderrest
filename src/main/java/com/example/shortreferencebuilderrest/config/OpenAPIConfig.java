package com.example.shortreferencebuilderrest.config;

import org.springframework.context.annotation.Configuration;

/**
 * Date: 29.03.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p>Used for create initial data for yaml file.</p>
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Configuration
public class OpenAPIConfig {
//    @Bean
//    public GroupedOpenApi publicUserApi() {
//        return GroupedOpenApi.builder()
//                .group("")
//                .pathsToMatch("/**")
//                .build();
//    }

//    @Bean
//    public OpenAPI customOpenApi(@Value("${application-description}") String appDescription,
//                                 @Value("${application-version}") String appVersion) {
//        EnvironmentUtil environmentUtil = new EnvironmentUtil();
//        return new OpenAPI().info(new Info().title("ShortReferenceBuilder")
//                        .version(appVersion)
//                        .description(appDescription)
//                        .license(new License().name("Apache 2.0")
//                                .url("http://springdoc.org"))
//                        .contact(new Contact().name("Vorogushin D.")
//                                .email("dvorogushin@gmail.com")))
//                .servers(List.of(new Server().url(environmentUtil.getServerUrlPrefi())
//                                .description("REST service")));
//    }
}

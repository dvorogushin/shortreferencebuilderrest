package com.example.shortreferencebuilderrest.controllers.modelAssembler;

import com.example.shortreferencebuilderrest.controllers.ReferenceApiController;
import com.example.shortreferencebuilderrest.controllers.RequestApiController;
import com.example.shortreferencebuilderrest.transfer.RequestInfoDto;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.methodOn;

/**
 * Date: 13.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * Implements HyperLinks to the self, to the parent {@code reference}, to the list of {@code references} and to the list of {@code requests}.
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Component
public class RequestDtoModelAssembler implements RepresentationModelAssembler<RequestInfoDto, EntityModel<RequestInfoDto>> {

    private Pageable pageable;
    
    @Override
    public EntityModel<RequestInfoDto> toModel(RequestInfoDto requestInfoDto) {
        return EntityModel.of(requestInfoDto,
                linkTo(methodOn(RequestApiController.class)
                        .getRequest(requestInfoDto.getId()))
                        .withSelfRel(),
                linkTo(methodOn(RequestApiController.class)
                        .getAllRequests(requestInfoDto.getReferenceId(), pageable))
                        .withRel("requests"),
                linkTo(methodOn(ReferenceApiController.class)
                        .getReferenceById(requestInfoDto.getReferenceId()))
                        .withRel("reference"),
                linkTo(methodOn(ReferenceApiController.class)
                        .getAllReferences(pageable))
                        .withRel("references"));

    }
}

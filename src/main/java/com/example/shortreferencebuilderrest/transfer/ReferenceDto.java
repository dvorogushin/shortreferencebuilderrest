package com.example.shortreferencebuilderrest.transfer;

import com.example.shortreferencebuilderrest.models.Reference;
import com.example.shortreferencebuilderrest.config.EnvironmentUtil;
import com.example.shortreferencebuilderrest.models.RequestInfo;
import com.example.shortreferencebuilderrest.models.User;
import com.example.shortreferencebuilderrest.services.RequestService;
import com.example.shortreferencebuilderrest.services.UserService;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.core.Relation;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Date: 13.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * Converts {@link Reference} entity to {@link ReferenceDto DTO} object, which is a representational object for the servlet response.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Component
// Used for Hateoas descriptions
@Relation(collectionRelation = "references", itemRelation = "reference")
public class ReferenceDto {

    /**
     * {@code Request} service as static field to use in static methods.
     */
    private static RequestService requestService;

    /**
     * {@code User} service as static field to use in static methods.
     */
    private static UserService userService;

    /**
     * Injection of services to the static fields.
     *
     * @param requestService
     * @param userService
     */
    @Autowired
    private ReferenceDto(RequestService requestService, UserService userService) {
        ReferenceDto.requestService = requestService;
        ReferenceDto.userService = userService;
    }
//    @Autowired
//    private ReferenceService service;
//
//    @PostConstruct
//    public void init() {
//        this.referenceService = service;
//    }

    /**
     * {@code Reference's} ID.
     */
    private Long id;

    /**
     * {@code Reference's} short name.
     */
    private String shortReference;

    /**
     * {@code Reference's} redirect URL.
     */
    private String redirectReference;

    /**
     * Date and time of {@code reference} creation.
     */
    private LocalDateTime creationDate;

    /**
     * Date ant time of {@code reference} expiration.
     */
    private LocalDateTime expirationDate;

    /**
     * {@code Reference's} state.
     */
    private String state;

    /**
     * Quantity of requests in total.
     */
    private Integer requestCount;

    /**
     * Quantity of redirects (successful requests) in total.
     */
    private Integer redirectCount;

    /**
     * Expiration flag.
     */
    private Boolean isExpired;

    /**
     * {@link User User}, who owns the {@code reference}.
     */
    private String ownerEmail;

    /**
     * Environment details.
     */
    private static final EnvironmentUtil invUtil = new EnvironmentUtil();

    /**
     * Converts one {@link Reference} item to {@link ReferenceDto DTO} object.
     *
     * @param reference {@link Reference} object.
     * @return {@link ReferenceDto} object.
     */
    public static ReferenceDto from(Reference reference) {

        // particular request to db because of FETCH.LAZY
        if (reference.getOwnerUser() != null) {
            reference.setOwnerUser(userService.getUser(reference.getOwnerUser().getId()));
        }

        // particular request to db because of FETCH.LAZY
        if (reference.getRequests() != null) {
            reference.setRequests(requestService.getRequests(reference.getId()));
        }

        return ReferenceDto.builder()
                .id(reference.getId())
                .shortReference(invUtil.getScheme() + "://" + invUtil.getHostName() + "/" + reference.getShortRef())
                .redirectReference(reference.getRedirectRef())
                .creationDate(reference.getCreationDateTime())
                .expirationDate(reference.getExpirationDateTime())
                .state(reference.getState().toString())
                .isExpired(Reference.ReferenceValidator.isExpired(reference))

                // FETCH.LAZY relations
                .ownerEmail(reference.getOwnerUser() != null ? reference.getOwnerUser().getEmail() : "")
                .requestCount(reference.getRequests() != null ? reference.getRequests().size() : 0)
                .redirectCount(reference.getRequests() != null ?
                        (int) reference.getRequests().stream()
                                .filter(RequestInfo::getRedirected)
                                .count()
                        : 0)
                .build();
    }

    /**
     * Converts collection of {@link Reference} to the collection of {@link ReferenceDto DTO} objects.
     *
     * @param references List of {@link Reference} objects.
     * @return List of {@link ReferenceDto DTO} objects.
     */
    public static List<ReferenceDto> from(List<Reference> references) {
        return references.stream().map(ReferenceDto::from).collect(Collectors.toList());
    }
}

package com.example.shortreferencebuilderrest.controllers;

import com.example.shortreferencebuilderrest.compare.ObjectComparator;
import com.example.shortreferencebuilderrest.services.RequestService;
import com.example.shortreferencebuilderrest.transfer.RequestInfoDto;
import com.example.shortreferencebuilderrest.controllers.modelAssembler.RequestDtoModelAssembler;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static com.example.shortreferencebuilderrest.transfer.ReferenceDto.from;

/**
 * Date: 13.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p>Controller for {@code Requests} data.
 * As per request's header {@code Accept} returns data in {@code json} or {@code xml} format.
 * </p>
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@RestController
@RequiredArgsConstructor
public class RequestApiController {

    @Autowired
    private final RequestService requestService;

    @Autowired
    private final RequestDtoModelAssembler assembler;

    // для гиперссылок пагинации
    private final PagedResourcesAssembler pagedResourcesAssembler;

    // получение всех запросов по ссылке
    /**
     * GET all
     * <p>Returns all {@code requests} (both successful and unsuccessful) to the certain {@code reference}, wrapped in to the pageable part.
     * Hyperlinks to self are used.</p>
     *
     * @param pageable page number, page size in items, sort property, direction. Not required.
     * @return List of {@link RequestInfoDto} objects.
     */
    @GetMapping("/references/{reference-id}/requests")
    public ResponseEntity getAllRequests(
            @PathVariable("reference-id") Long referenceId,
            @PageableDefault(page = 0, size = 2, sort = "id", direction = Sort.Direction.ASC)
                    Pageable pageable) {

        // формирование списка реквестов в формате Dto,
        // для применения сортировки по полям RequestInfoDto
        List<RequestInfoDto> requestInfoDtoList = RequestInfoDto.from(requestService.getRequests(referenceId));
        // multysorting collection as per PageRequest
        requestInfoDtoList.sort(new ObjectComparator<>(pageable, RequestInfoDto.class));

        // пагинация полученного списка путем выделения подсписка (без сортировки!!!)
        int start = (int) pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), requestInfoDtoList.size());
        // если параметры выходят за область страницы
        if (start > end) {
            start = 0; end = 0;
        }
        final Page<RequestInfoDto> pagedRequestInfoDto = new PageImpl<>(
                requestInfoDtoList.subList(start, end),
                pageable,
                requestInfoDtoList.size());

//        // получение страницы запросов из базы в соответствии с запросом Pageable
//        Page<RequestInfo> pagedRequests = requestService.getRequests(referenceId, pageable);

        List<EntityModel<RequestInfoDto>> requestInfoDtoEntityModelList =
                        (pagedRequestInfoDto.getContent())
                        .stream()
                        .map(assembler::toModel)
                        .collect(Collectors.toList());

        // формирование страницы с данными о ней
        Page<EntityModel<RequestInfoDto>> pagedEntity =
                new PageImpl<>(requestInfoDtoEntityModelList,
                        pageable,
                        pagedRequestInfoDto.getTotalElements());

        // формирование гиперссылок страницы (first/last/prev/next/self)
        return ResponseEntity
                .ok()
//                .contentType(MediaTypes.HAL_JSON)
                .body(pagedResourcesAssembler.toModel(pagedEntity));
    }

    // вариант гиперссылок без пагинации
//    @GetMapping("/references/{reference-id}/requests")
//    public CollectionModel<EntityModel<RequestInfoDto>> getAll(@PathVariable("reference-id") Long referenceId) {
//        List<EntityModel<RequestInfoDto>> requests =
//                RequestInfoDto.from(requestService.getRequests(referenceId)).stream()
//                        .map(assembler::toModel)
//                        .collect(Collectors.toList());
//        return CollectionModel.of(requests,
//                linkTo(methodOn(RequestApiController.class)
//                        .getAll(referenceId))
//                        .withSelfRel());
//    }

    /**
     * GET one
     * <p>Returns data for the one request.
     * Hyperlinks to self are present.</p>
     * @param requestId ID of the {@code request}.
     * @return {@link RequestInfoDto} object.
     */
    @GetMapping("/requests/{request-id}")
    public EntityModel<RequestInfoDto> getRequest(@PathVariable("request-id") Long requestId) {
        return assembler.toModel(RequestInfoDto.from(requestService.getRequest(requestId)));
    }
}

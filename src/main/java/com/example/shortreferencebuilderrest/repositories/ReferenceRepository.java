package com.example.shortreferencebuilderrest.repositories;

import com.example.shortreferencebuilderrest.models.Reference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Date: 13.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * Reference's JPA repository.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Repository
public interface ReferenceRepository extends JpaRepository<Reference, Long>, PagingAndSortingRepository<Reference, Long> {

//    //language=SQL
//    @Query(value = "SELECT * FROM reference r join request req ON req.reference_id = (:idd) where r.id = (:idd)", nativeQuery = true)
    Optional<Reference> findByIdOrderByIdAsc(@Param("idd") Long shortReference);

    Page<Reference> findAll(Pageable pageable);

    List<Reference> findByOrderByIdAsc();
}

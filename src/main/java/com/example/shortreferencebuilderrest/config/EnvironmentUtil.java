package com.example.shortreferencebuilderrest.config;


import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * Date: 06.03.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 *
 * <p>Utility class for environment representation: scheme, server name, port.
 * Data is taken from the <a href="/resources/application.properties">properties</a> file.</p>
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Configuration
public class EnvironmentUtil implements EnvironmentAware {

    public Environment environment;

    @Override
    public void setEnvironment(final Environment environment) {
        this.environment = environment;
    }

    private static String scheme;
    private static String port;
    private static String hostname;

    /**
     * @return port of application as string.
     */
    @Bean
    public String getPort() {
        if (port == null) port = environment.getProperty("server.port");
        return port;
    }

    /**
     * @return port of application as integer.
     */
    @Bean
    public Integer getPortAsInt() {
        return Integer.valueOf(getPort());
    }

    /**
     * @return server's host name as string.
     */
    @Bean
    public String getHostName(){
        if (hostname == null) {
            hostname = environment.getProperty("server.name");
        }
        return hostname;
    }

    /**
     * @return server's scheme as string.
     */
    @Bean
    public String getScheme() {
        if (scheme == null) scheme = environment.getProperty("server.scheme");
        return scheme;
    }


    /**
     * @return server's URL prefix as string.
     */
    @Bean
    public String getServerUrlPrefi()  {
        return getScheme() + "://" + getHostName() + ":" + getPort();
    }
}


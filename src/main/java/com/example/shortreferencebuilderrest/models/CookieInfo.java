package com.example.shortreferencebuilderrest.models;

import lombok.*;

import javax.persistence.*;

/**
 * Date: 13.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * The {@code Cookies} entity. Is used to deal with cookies of each redirecting {@code request}.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "cookie")
public class CookieInfo {

    /**
     * Cookie ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * {@code Request} ID.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "request_id", columnDefinition = "BIGINT NOT NULL")
    private RequestInfo requestInfo;

    /**
     * {@code Cookie}'s name field.
     */
    private String name;

    /**
     * {@code Cookie}'s value field.
     */
    private String value;

}

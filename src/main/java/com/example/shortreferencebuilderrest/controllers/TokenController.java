package com.example.shortreferencebuilderrest.controllers;

import com.example.shortreferencebuilderrest.security.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Date: 06.05.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p>
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@RestController
public class TokenController {

    @Autowired
    private SessionService tokenService;

    @GetMapping("/api")
    public ResponseEntity getTokens() {

        return ResponseEntity.ok().body(tokenService.getAll());
    }
}
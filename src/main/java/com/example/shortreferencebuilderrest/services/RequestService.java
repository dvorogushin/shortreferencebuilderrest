package com.example.shortreferencebuilderrest.services;

import com.example.shortreferencebuilderrest.models.RequestInfo;

import java.util.List;

/**
 * Date: 13.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * Service interface for operations with {@code requests}.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
public interface RequestService {

    List<RequestInfo> getRequests(Long referenceId);

    RequestInfo getRequest(Long referenceId);
}

package com.example.shortreferencebuilderrest.security.config;

import com.example.shortreferencebuilderrest.security.details.UserDetailsServiceImpl;
import com.example.shortreferencebuilderrest.security.filter.RefreshTokenFilter;
import com.example.shortreferencebuilderrest.security.filter.TokenAuthenticationFilter;
import com.example.shortreferencebuilderrest.security.filter.TokenAuthorizationFilter;
import com.example.shortreferencebuilderrest.security.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Date: 25.04.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p>
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    public static final String LOGIN_FILTER_PROCESSES_URL = "/authorize";
    public static final String REFRESH_FILTER_URL = "/refresh";
    public static final String TOKEN_TYPE = "token_type";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String REFRESH_TOKEN = "refresh_token";
    public static final String EXPIRE_IN = "expire_in";
    public static final String TOKEN_TYPE_BEARER = "Bearer";
    public static final String CACHE_CONTROL = "Cache-Control";
    public static final String CACHE_CONTROL_NO_STORE = "no-store";
    public static final String PRAGMA = "Pragma";
    public static final String PRAGMA_NO_CACHE = "no-cache";

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }



    @Override
    protected void configure(HttpSecurity http) throws Exception {

        TokenAuthenticationFilter tokenAuthenticationFilter =
                new TokenAuthenticationFilter(authenticationManagerBean(), sessionService);
        tokenAuthenticationFilter.setFilterProcessesUrl(LOGIN_FILTER_PROCESSES_URL);


        http

                .cors().and() // for Swagger UI
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                // Password credentials -> tokens
                .addFilter(tokenAuthenticationFilter)
                // Access token -> pass resource
                .addFilterAfter(new TokenAuthorizationFilter(sessionService),
                        TokenAuthenticationFilter.class)
                // Refresh token -> new tokens
                .addFilterBefore(new RefreshTokenFilter(sessionService),
                        TokenAuthorizationFilter.class);


        http.authorizeRequests()
                .antMatchers("/api").permitAll() // TEST: Get all sessions
                .antMatchers(REFRESH_FILTER_URL).permitAll()
                .antMatchers(LOGIN_FILTER_PROCESSES_URL).permitAll()
                .antMatchers(HttpMethod.GET,"/references").permitAll()
                .antMatchers(HttpMethod.POST,"/references").authenticated()
                .antMatchers("/references/**").authenticated()
                .antMatchers("/requests/**").authenticated();
//                .antMatchers(HttpMethod.PUT, STUDENTS_API).hasAuthority("ADMIN")
//                .antMatchers(HttpMethod.DELETE, STUDENTS_API).hasAuthority("ADMIN")
//                .antMatchers(HttpMethod.GET, STUDENTS_API).authenticated();
    }
}
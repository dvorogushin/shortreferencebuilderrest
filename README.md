# ShortReferenceBuilderREST

## Description
This simple server realizes REST API architecture service for [MVC application](https://gitlab.com/dvorogushin/shortreferencebuilder), deployed at [qzee.ru](http://qzee.ru).

## Structure
### Controllers
- ***[Reference](src/main/java/com/example/shortreferencebuilderrest/controllers/ReferenceApiController.java)***
  <br>Provides access to the `reference` entity.
  <br>Realizes all CRUD operations.
- ***[Request](src/main/java/com/example/shortreferencebuilderrest/controllers/RequestApiController.java)***
  <br>Provides access to the `request` entity.
  <br>Realizes only Read operation (GET methods).
### Entities
- ***[Reference](src/main/java/com/example/shortreferencebuilderrest/models/Reference.java)***
  <br>Describes a randomly generated short link.
  <br>Includes a URL for redirection, creation and expiration dates etc.
- ***[Request](src/main/java/com/example/shortreferencebuilderrest/models/RequestInfo.java)***
  <br>Describes the request to the short link for redirection.
  <br>Includes the request's features, such as IP, some headers, cookies, geographic coordinates, result of redirection etc.
- ***[Session](src/main/java/com/example/shortreferencebuilderrest/security/model/Session.java)***
  <br>Consists of a keys only, which are used to encrypt-decrypt the JWT. 
### Response
- Body is built in `json` or `xml` format as per request's `Accept` header.
- Is described in the [yaml](http://qzee.ru/api/v3/openapi.yaml) / [json](http://qzee.ru/api/v3/openapi.json) files in OpenAPI 3.0 format.
- Can be checked and tested in [Swagger UI](http://qzee.ru/api/v3/swagger-ui) application.

### Authorization
- Resource owner password credentials flow realized (OAuth2.0).

## Features
### Tokens
The user can have unlimited number of client-sessions.<br> 
After authorization, the details of each session are not stored on the server side, but encrypted in JWT, which is used as an access token.
The server only stores encrypt and signature keys at in-memory database (HSQLDB).

### Pagination
When the server's response includes a collection of items (GET all), the collection can be partitioned on a pages with any number of items in it. Each page includes a hyperlinks to itself, previous, next, first and last pages.<br>
The collection can be sorted in any order and direction.<br>
Pagination parameters should be added to a query. For example: 
```
path?page=0&size=2&sort=creationDate,ASC&sort=id,DESC
```
### Hypermedia
Each successful response from the server includes a hyperlinks to the items (HATEOAS). There are hyperlinks to itself, to the item's collection and to the collection of relating items.
<br>For example: inside server's response each `reference` item includes the hyperlinks to itself, to the collection of `references` and to the collection of corresponding `requests` for redirection through this `reference`. 

## Deployment
Current version of the server is deployed on testing [host](http://qzee.ru:8081/references).

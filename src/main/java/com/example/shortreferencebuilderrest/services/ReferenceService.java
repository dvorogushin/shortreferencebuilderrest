package com.example.shortreferencebuilderrest.services;

import com.example.shortreferencebuilderrest.forms.ReferenceForm;
import com.example.shortreferencebuilderrest.models.Reference;
import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * Date: 13.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * Service interface for operations with {@code references}.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
public interface ReferenceService {

    Reference getReference(Long referenceId);

//    List<Reference> getAll();
    List<Reference> getAll();

    Reference createReference(ReferenceForm form);

    void deleteReference(Long referenceId);

    Reference changeState(Long referenceId);
}

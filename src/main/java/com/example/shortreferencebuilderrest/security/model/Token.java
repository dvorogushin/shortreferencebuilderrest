package com.example.shortreferencebuilderrest.security.model;

import com.example.shortreferencebuilderrest.config.EnvironmentUtil;
import com.example.shortreferencebuilderrest.exceptions.BadTokenException;
import com.example.shortreferencebuilderrest.models.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import lombok.*;
import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jwk.OctetSequenceJsonWebKey;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.AesKey;
import org.jose4j.lang.JoseException;

import java.io.Serializable;
import java.security.Key;

/**
 * Date: 11.05.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p> Creates, encrypts and decrypts JWTs.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Getter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class Token implements Serializable {

    public static final int EXPIRATION_TIME_MINUTES_IN_THE_FUTURE_ACCESS_TOKEN = 120;
    public static final int EXPIRATION_TIME_MINUTES_IN_THE_FUTURE_REFRESH_TOKEN = 1440; // twenty-four hours
    public static final String CLAIM_NAME_EMAIL = "email";
    public static final String CLAIM_NAME_ROLE = "role";
    public static final String CLAIM_NAME_STATE = "state";
    public static final String CLAIM_NAME_TYPE = "type";

    /**
     * Type of the token.
     */
    public enum Type {
        ACCESS,
        REFRESH
    }

    /**
     * Client's session.
     */
    private final Session session;
    private User user;
    @JsonProperty(value = "type")
    private Type type;
    private String encryptedValue;
    private Long expirationTime;

    /**
     * Encrypts the token.
     */
    public void encrypt() {

        if (session == null || user == null || type == null) {
            throw new NullPointerException("Error while encrypting the token: TokenSession|User|Type couldn't be null.");
        }
        String SessionIdAsString = session.getId().toString();
        // create key for encrypting algorithm
        Key encryptKey = new AesKey(Session.asBytes(session.getEncValue()));
        // create key for signature algorithm
        Key signatureKey = new AesKey(Session.asBytes(session.getSigValue()));
        //  create JWK from key
        JsonWebKey jsonWebKey = new OctetSequenceJsonWebKey(encryptKey);
        // set key_id as session_id
        jsonWebKey.setKeyId(SessionIdAsString);

        // create the Claims, which are the content of the JWS
        JwtClaims claims = new JwtClaims();
        claims.setIssuer(new EnvironmentUtil().getHostName());
        switch (type) {
            case ACCESS:
                claims.setExpirationTimeMinutesInTheFuture(EXPIRATION_TIME_MINUTES_IN_THE_FUTURE_ACCESS_TOKEN);
                break;
            case REFRESH:
                claims.setExpirationTimeMinutesInTheFuture(EXPIRATION_TIME_MINUTES_IN_THE_FUTURE_REFRESH_TOKEN);
                break;
            default:
                claims.setExpirationTimeMinutesInTheFuture(0);
        }
        claims.setJwtId(SessionIdAsString);
//        claims.setGeneratedJwtId();
        claims.setIssuedAtToNow();
        claims.setNotBeforeMinutesInThePast(2);
        claims.setSubject(user.getId().toString());
        claims.setStringClaim(CLAIM_NAME_EMAIL, user.getEmail());
        claims.setStringClaim(CLAIM_NAME_ROLE, user.getRole().toString());
        claims.setStringClaim(CLAIM_NAME_STATE, user.getState().toString());
        claims.setStringClaim(CLAIM_NAME_TYPE, this.getType().toString());

        // encode the JWS with signatureKey
        JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(claims.toJson());
        jws.setKeyIdHeaderValue(SessionIdAsString);
//        jws.setKey(new HmacKey(ByteUtil.randomBytes(16)));
        jws.setKey(signatureKey);
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.HMAC_SHA256);
        jws.setDoKeyValidation(false);

        // create JWT, insert the JWS to JWT
        String jwt = null;
        try {
            jwt = jws.getCompactSerialization();
        } catch (JoseException e) {
            e.printStackTrace();
        }

        // create a JWE, which is the encrypted JWT
        JsonWebEncryption jwe = new JsonWebEncryption();
        jwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.A128KW);
//        jwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.DIRECT);
        jwe.setEncryptionMethodHeaderParameter(ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256);
        jwe.setKey(encryptKey);
        jwe.setKeyIdHeaderValue(SessionIdAsString);
        jwe.setContentTypeHeaderValue("JWT");
        jwe.setPayload(jwt);

        // encrypt JWT
        String jweSerialization = null;
        try {
            jweSerialization = jwe.getCompactSerialization();
        } catch (JoseException e) {
            e.printStackTrace();
        }

        // fill the fields: expiration time and encrypted value
        try {
            this.expirationTime = claims.getExpirationTime().getValue();
        } catch (MalformedClaimException e) {
            e.printStackTrace();
        }
        this.encryptedValue = jweSerialization;
    }

    /**
     * Decrypt the token.
     */
    public void decrypt() {

        if (session == null || encryptedValue == null) {
            throw new NullPointerException("Error while decrypt the token: TokenSession | encryptedValue couldn't be null.");
        }
        // read the keys from repository (in-memory db?)
        Key encryptKey = new AesKey(Session.asBytes(session.getEncValue()));
        Key signatureKey = new AesKey(Session.asBytes(session.getSigValue()));

        // Validate Token's authenticity and check claims
        JwtConsumer jwtConsumer = new JwtConsumerBuilder()
                .setRequireExpirationTime()
                .setAllowedClockSkewInSeconds(30)
                .setRequireSubject()
                .setRelaxDecryptionKeyValidation()
                .setRelaxVerificationKeyValidation()
                .setExpectedIssuer(new EnvironmentUtil().getHostName())
                .setDecryptionKey(encryptKey)
                .setVerificationKey(signatureKey)
                .build();
        JwtClaims jwtClaims = new JwtClaims();

        // Validate the JWT and process it to the Claims
        try {
            jwtClaims = jwtConsumer.processToClaims(encryptedValue);
        } catch (InvalidJwtException e) {
            e.printStackTrace();
            throw new BadTokenException();
        }

        // create user from the token claims
        User user = new User();
        ObjectMapper objectMapper = new JsonMapper();
        try {
            user = objectMapper.readValue(jwtClaims.getRawJson(), User.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        String type = (String) jwtClaims.getClaimValue(CLAIM_NAME_TYPE);
        if (type.equals(Type.ACCESS.toString())) {
            this.type = Type.ACCESS;
        } else if (type.equals(Type.REFRESH.toString())) {
            this.type = Type.REFRESH;
        } else throw new BadTokenException();

        this.user = user;
    }
}
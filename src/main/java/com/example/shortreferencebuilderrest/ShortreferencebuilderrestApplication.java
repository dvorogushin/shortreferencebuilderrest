package com.example.shortreferencebuilderrest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
//        (exclude = SecurityAutoConfiguration.class)
public class ShortreferencebuilderrestApplication {
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    // для разблокировки функции CROS-requests
    // актуально только для отправки запросов из SwaggerUI из браузера
    @Value("${cors.origin.client}")
    String corsClient;
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/references/**")
                        .allowedOrigins(corsClient)
                        .allowedMethods("GET", "POST", "PUT", "DELETE")
//                        .allowedHeaders("header1", "header2", "header3")
//                        .exposedHeaders("header1", "header2")
                        .allowCredentials(false)
                        .maxAge(3600);
                registry.addMapping("/requests/**")
                        .allowedOrigins(corsClient)
                        .allowedMethods("GET")
                        .allowCredentials(false).maxAge(3600);
                registry.addMapping("/refresh")
                        .allowedOrigins(corsClient)
                        .allowedMethods("GET")
                        .allowCredentials(false).maxAge(3600);
                registry.addMapping("/authorize")
                        .allowedOrigins(corsClient)
//                        .allowedHeaders("Content-Type", "X-Requested-With", "accept", "Origin", "Access-Control-Request-Method",
//                                "Access-Control-Request-Headers")
//                        .exposedHeaders("Access-Control-Allow-Origin", "Access-Control-Allow-Credentials")
                        .allowedMethods("POST")
                        .allowCredentials(false).maxAge(3600);
            }
        };
    }



    public static void main(String[] args) {
        SpringApplication.run(ShortreferencebuilderrestApplication.class, args);
    }

}

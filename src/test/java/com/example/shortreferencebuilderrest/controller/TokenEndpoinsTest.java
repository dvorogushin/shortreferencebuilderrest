package com.example.shortreferencebuilderrest.controller;

import com.example.shortreferencebuilderrest.models.User;
import com.example.shortreferencebuilderrest.security.details.UserDetailsImpl;
import com.example.shortreferencebuilderrest.security.details.UserDetailsServiceImpl;
import com.example.shortreferencebuilderrest.security.filter.TokenAuthenticationFilter;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.aggregator.ArgumentAccessException;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Date: 03.06.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p>
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@DisplayName("/authorize end point is being tested")
@AutoConfigureMockMvc
//@ExtendWith(SpringExtension.class)
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)   // for @BeforeAll
@Slf4j
public class TokenEndpoinsTest {

    @Autowired
    private MockMvc mockMvc;

    @SpyBean
//            @MockBean
    TokenAuthenticationFilter tokenAuthenticationFilter;

    UserDetails userDetails = new UserDetailsImpl(
            User.builder()
                    .email("admin@gmailll.com")
                    .state(User.State.CONFIRMED)
                    .role(User.Role.USER)
                    .build());
    Authentication authentication =
            new UsernamePasswordAuthenticationToken(
                    "admin@gmaill.com", "qwerty007", userDetails.getAuthorities());

    @Test
    public void whenCredentials_thenTokens() throws Exception {
log.info("Test start. No mock method.");
log.info("Perform");
        ResultActions resultActions = mockMvc.perform(post("/authorize")
                        .content("{\"email\": \"admin@gmail.com\",\"password\": \"qwerty007\"}")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(200));
        log.info("End test");
        verify(tokenAuthenticationFilter).attemptAuthentication(ArgumentMatchers.any(HttpServletRequest.class),
                ArgumentMatchers.any(HttpServletResponse.class));

    }
    @Test
    public void whenCredentials_DoReturn_thenTokens() throws Exception {
log.info("Test start. DoReturn.");
doReturn(authentication).when(tokenAuthenticationFilter).attemptAuthentication(
        ArgumentMatchers.any(HttpServletRequest.class),
        ArgumentMatchers.any(HttpServletResponse.class));

log.info("Perform");
        ResultActions resultActions = mockMvc.perform(post("/authorize")
                        .content("{\"email\": \"admin@gmail.com\",\"password\": \"qwerty007\"}")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(200));
        log.info("End test");
verify(tokenAuthenticationFilter).attemptAuthentication(ArgumentMatchers.any(HttpServletRequest.class),
        ArgumentMatchers.any(HttpServletResponse.class));
    }

    @Test
    public void whenCredentials_When_thenTokens() throws Exception {
log.info("Test start. When");
when(tokenAuthenticationFilter.attemptAuthentication(
        ArgumentMatchers.any(HttpServletRequest.class),
        ArgumentMatchers.any(HttpServletResponse.class))).thenReturn(authentication);

log.info("Perform");
        ResultActions resultActions = mockMvc.perform(post("/authorize")
                        .content("{\"email\": \"admin@gmail.com\",\"password\": \"qwerty007\"}")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(200));
        log.info("End test");
        verify(tokenAuthenticationFilter).attemptAuthentication(ArgumentMatchers.any(HttpServletRequest.class),
                ArgumentMatchers.any(HttpServletResponse.class));

    }
    @Test
    public void whenCredentials_Given_thenTokens() throws Exception {
log.info("Test start. Given.");
        given(tokenAuthenticationFilter.attemptAuthentication(
                ArgumentMatchers.any(HttpServletRequest.class),
                ArgumentMatchers.any(HttpServletResponse.class))).willReturn(authentication);

log.info("Perform");
        ResultActions resultActions = mockMvc.perform(post("/authorize")
                        .content("{\"email\": \"admin@gmail.com\",\"password\": \"qwerty007\"}")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(200));
        log.info("End test");
        verify(tokenAuthenticationFilter).attemptAuthentication(ArgumentMatchers.any(HttpServletRequest.class),
                ArgumentMatchers.any(HttpServletResponse.class));

    }

}
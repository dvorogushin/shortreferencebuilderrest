package com.example.shortreferencebuilderrest.models;

import lombok.*;
import org.springframework.transaction.annotation.Propagation;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Date: 13.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * The {@code Reference} entity.
 * Combines main parameters and methods of a short link ({@code reference}).
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "reference")
public class Reference {

    /**
     * Maximum length of the {@code reference} name.
     */
    public static final Integer SHORTREFERENCE_MAXSIZE = 8;

    /**
     * Default value for {@code freePeriod} field.
     * Is used to count expiration date when new {@code reference} has been creating.
     */
    public static final Integer DEFAULT_FREE_PERIOD_DAYS = 5;

    /**
     * Permitted symbols for short {@code reference's} name.
     */
    public static final String SHORTREFERENCE_ALLOWED_CHARS =
            "1234567890" +
                    "ABCDEFGHIJKLMNQRSTUVWXYZ" +
                    "abcdefghijklmnopqrstuvwxyz" +
                    "-_";

    /**
     * Available {@code reference's} state.
     */
    public enum State {
        ACTIVE, NOT_ACTIVE;
    }

    /**
     * {@code Reference's} ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Date and time of {@code reference} creation.
     */
    @Column(columnDefinition = "TIMESTAMP NOT NULL", name = "creation_date_time")
    private LocalDateTime creationDateTime;

    /**
     * Date ant time of {@code reference} expiration.
     */
    @Column(columnDefinition = "TIMESTAMP NOT NULL", name = "expiration_date_time")
    private LocalDateTime expirationDateTime;

    /**
     * {@code Reference's} short name.
     */
    @Column(columnDefinition = "VARCHAR (20) NOT NULL", name = "short_ref")
    private String shortRef;

    /**
     * {@code Reference's} redirect URL.
     */
    @Column(columnDefinition = "VARCHAR NOT NULL", name = "redirect_ref")
    private String redirectRef;

    /**
     * {@code Reference's} state.
     */
    @Enumerated(value = EnumType.STRING)
    @Column(columnDefinition = "VARCHAR (20) NOT NULL")
    private State state;

    /**
     * {@link User}, who owns the {@code reference}.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_user_id", columnDefinition = "BIGINT")
    private User ownerUser;

    /**
     * List of {@link RequestInfo requests} for redirection to the redirect URL.
     */
    @OneToMany(
            mappedBy = "reference",
            cascade = CascadeType.REMOVE,
            fetch = FetchType.LAZY)
    @ToString.Exclude
    private List<RequestInfo> requests;

    /**
     * Invokes {@code test()} method of Predicate functional interface.
     */
    static public <T> boolean validate(Predicate<T> predicate, T t) {
        return predicate.test(t);
    }

    /**
     * Invokes {@code get()} method of Supplier functional interface.
     */
    static public String create(Supplier<String> supplier) {
        return supplier.get();
    }

    /**
     * Combines {@code reference's} validation methods.
     */
    static public class ReferenceValidator {

        /**
         * Checks the {@code short name} for allowed symbols.
         *
         * @param shortReference short name
         * @return true if all symbols are matching pattern.
         */
        static public boolean checkChars(String shortReference) {
            Boolean result = false;

            if (shortReference != null
                    && shortReference.length() <= SHORTREFERENCE_MAXSIZE
                    && shortReference.length() > 0) {
                for (int i = 0; i < shortReference.length(); i++) {
                    if (SHORTREFERENCE_ALLOWED_CHARS.indexOf(shortReference.charAt(i)) < 0) {
                        return false;
                    }
                    result = true;
                }
            }
            return result;
        }


        /**
         * Checks if a date & time of the {@code reference} are expired.
         *
         * @param reference {@link Reference} object.
         * @return true if the date & time are expired.
         */
        static public boolean isExpired(Reference reference) {
            return LocalDateTime.now().isAfter(reference.getExpirationDateTime());
        }
    }

    /**
     * Combines the methods for creation of the {@code references}
     */
    static public class ReferenceCreator {


        /**
         * Randomly creates a name of a new {@code reference} - {@link Reference#shortRef} field.
         *
         * @return
         */
        public static String createShortReference() {
            StringBuilder str = new StringBuilder();
            char[] chars = SHORTREFERENCE_ALLOWED_CHARS.toCharArray();
            Random rand = new Random();
            for (int i = 0; i < SHORTREFERENCE_MAXSIZE; i++) {
                shuffleArray(chars);
                str.append(chars[rand.nextInt(chars.length)]);
            }
            return str.toString();
        }

        /**
         * Shuffles the symbols' array after generation of each symbol.
         *
         * @param array symbols
         */
        static void shuffleArray(char[] array) {
            Random rnd = ThreadLocalRandom.current();
            int index;
            char temp;
            for (int i = array.length - 1; i > 0; i--) {
                index = rnd.nextInt(i + 1);
                temp = array[index];
                array[index] = array[i];
                array[i] = temp;
            }
        }
    }

}

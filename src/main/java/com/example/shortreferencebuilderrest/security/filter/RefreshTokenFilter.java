package com.example.shortreferencebuilderrest.security.filter;

import com.example.shortreferencebuilderrest.security.model.Token;
import com.example.shortreferencebuilderrest.security.service.SessionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import static com.example.shortreferencebuilderrest.security.config.SecurityConfiguration.*;
import static org.springframework.util.ObjectUtils.isEmpty;

/**
 * Date: 16.05.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p> Filter for "/refresh" endpoint.
 * Validates a refresh_token. Returns new access and refresh tokens.
 * Updates authenticated session with new encrypt and signature keys .
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@WebFilter("/refresh")
public class RefreshTokenFilter extends OncePerRequestFilter {

    private ObjectMapper objectMapper = new ObjectMapper();


    private SessionService sessionService;

    public RefreshTokenFilter(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    /**
     * Validates a refresh_token. Returns new access and refresh tokens.
     * Updates authenticated session with new encrypt and signature keys .
     *
     * @param request
     * @param response
     * @param filterChain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        // if request to "/refresh" endpoint
        if (request.getRequestURI().equals(REFRESH_FILTER_URL)) {
            String errorMessage = "";
            // try find, check, read, validate the token
            try {
                errorMessage = "invalid_request";
                String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
                if (!isEmpty(authHeader) && authHeader.startsWith(TOKEN_TYPE_BEARER)) {
                    // retrieve encrypted token from the header
                    String encryptedToken = authHeader.substring(TOKEN_TYPE_BEARER.length() + 1);
                    // decrypt the token
                    Token decryptedToken = sessionService.getDecryptedToken(encryptedToken);
                    // if the token is refresh one
                    if (decryptedToken.getType().equals(Token.Type.REFRESH)) {
                        Map<String, String> responseMap = new HashMap<>();
                        // if the token is valid then get new access and refresh tokens
                        Set<Token> tokens = sessionService.refreshTokens(decryptedToken);
                        // insert access token to the response
                        for (Token token : tokens) {
                            if (token.getType().equals(Token.Type.ACCESS)) {
                                responseMap.put(ACCESS_TOKEN, token.getEncryptedValue());
                                responseMap.put(TOKEN_TYPE, TOKEN_TYPE_BEARER);
                                responseMap.put(
                                        EXPIRE_IN,
                                        String.valueOf(
                                                ChronoUnit.SECONDS.between(
                                                        LocalDateTime.now(),
                                                        LocalDateTime.ofInstant(
                                                                Instant.ofEpochSecond(token.getExpirationTime()),
                                                                TimeZone.getDefault().toZoneId()))));
                            }
                        }
                        // insert refresh token to the response
                        for (Token token : tokens) {
                            if (token.getType().equals(Token.Type.REFRESH) && responseMap.containsKey(ACCESS_TOKEN)) {
                                responseMap.put(REFRESH_TOKEN, token.getEncryptedValue());
                            }
                        }
                        // OpenID Connect Core 1.0 (3.1.3.3.)
                        response.setHeader(CACHE_CONTROL, CACHE_CONTROL_NO_STORE);
                        response.setHeader(PRAGMA, PRAGMA_NO_CACHE);
                    // send the response
                    objectMapper.writeValue(response.getWriter(), responseMap);
                    } else {
                        errorMessage = "invalid_grant";
                        return;
                    }
//                    objectMapper.writeValue(response.getWriter(), responseMap);
                }
            } finally {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, errorMessage);
                return;
            }
        }
        filterChain.doFilter(request, response);
    }
}

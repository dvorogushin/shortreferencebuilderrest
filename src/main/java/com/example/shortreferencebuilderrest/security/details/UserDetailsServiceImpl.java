package com.example.shortreferencebuilderrest.security.details;

import com.example.shortreferencebuilderrest.models.User;
import com.example.shortreferencebuilderrest.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Date: 25.04.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * A part of Spring Security. Defines a conditions of user's identification.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@RequiredArgsConstructor
@Service
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {

    /**
     * User's repository object.
     */
    private final UserRepository userRepository;

    /**
     * Identifies a user by email.
     * Email is a unique user's identifier for any user's {@link User.State state}
     * except {@link User.State#DELETED DELETED} state.
     * Identifier for {@link User.State#DELETED DELETED} accounts is absent.
     * Any quantity of {@link User.State#DELETED DELETED} accounts can have the same email.
     *
     * @param email user's email as string.
     * @return the {@code User} wrapped in {@link UserDetails} object.
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        // all users
        List<User> usersAll = userRepository.findAllByEmail(email);

        // not deleted users
        List<User> usersNotDeleted = usersAll.stream()
                .filter(u -> u.getState() != (User.State.DELETED))
                .collect(Collectors.toList());

        // unique user
        if (usersNotDeleted.size() == 1) {
            return new UserDetailsImpl(usersNotDeleted.stream()
                    .findFirst()
                    .orElseThrow(() -> {
                        log.info("User {} not found", email);
                        return new UsernameNotFoundException("User not found.");
                    }));
        }
        log.info("User {} not found", email);
        throw new UsernameNotFoundException("User not found.");
    }
}

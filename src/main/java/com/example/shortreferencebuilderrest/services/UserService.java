package com.example.shortreferencebuilderrest.services;

import com.example.shortreferencebuilderrest.models.User;

/**
 * Date: 05.05.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p>
 * Service interface for operations with {@code User}.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
public interface UserService {
    User getUser(Long userId);
}
package com.example.shortreferencebuilderrest;

import com.example.shortreferencebuilderrest.config.EnvironmentUtil;
import com.example.shortreferencebuilderrest.transfer.ReferenceDto;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.http.converter.xml.MarshallingHttpMessageConverter;
//import org.springframework.oxm.xstream.XStreamMarshaller;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ShortreferencebuilderrestApplicationTests {

     @Autowired
     private EnvironmentUtil invUtil;

    @Test
    @Disabled
    void contextLoads() {
    }

    @Test
    @Disabled
    public void testGetMethodReferences() {
        String url = invUtil.getServerUrlPrefi() + "/references";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Object> reference = restTemplate.getForEntity(url, Object.class);
        assertEquals(HttpStatus.OK, reference.getStatusCode());
    }

    @Test
    @Disabled
    public void testGetReference5() {
        String url = invUtil.getServerUrlPrefi() + "/references/{reference-id}";
        RestTemplate restTemplate = new RestTemplate();
        ReferenceDto reference = restTemplate.getForObject(url, ReferenceDto.class, "5");
        assertEquals(Long.valueOf(5), reference.getId());
    }

//    @Test
//    public void testCrudMethods() {
//        String url = invUtil.getServerUrlPrefi() + "/references";
//        @NoArgsConstructor
//        class TestForm{
//            String url = "https://google.com";
//            Integer freePeriod = 4;
//        }
//        TestForm testForm = new TestForm();
//        RestTemplate restTemplate = new RestTemplate();
//        ResponseEntity<Object> reference = restTemplate.postForEntity(url, testForm, Object.class);
//        assertEquals(HttpStatus.OK, reference.getStatusCode());
//    }
//
    // Запрос на XML - ответ 406, NotAcceptable
    // чтобы сервер отдавал xml:
    // в pom.xml: jackson-dataformat-xml, либо
    // над контроллером: @XmlRootElement (2 разные библиотеки)
    // но все равно Error while extracting response, Could not unmarshal
    // нет подходящего конвектора для десериализации
    @Test
@Disabled
    public void givenConsumingXml_whenReadingTheReferenceDto_thenCorrect() {
        String url = invUtil.getServerUrlPrefi() + "/references/{reference-id}";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setMessageConverters(getMessageConverters());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", String.valueOf(MediaType.APPLICATION_XML));
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<ReferenceDto> response = restTemplate.exchange(url, HttpMethod.GET, entity, ReferenceDto.class, "5");
        ReferenceDto resource = response.getBody();
        assertThat(resource, notNullValue());
    }
    private List<HttpMessageConverter<?>> getMessageConverters() {
//        XStreamMarshaller marshaller = new XStreamMarshaller();
//        MarshallingHttpMessageConverter marshallingConverter = new MarshallingHttpMessageConverter(marshaller);
        List<HttpMessageConverter<?>> converters = new ArrayList<HttpMessageConverter<?>>();
//        converters.add(marshallingConverter);
//         to JSON, взято из следующего теста
        converters.add(new MappingJackson2HttpMessageConverter());
        converters.add(new Jaxb2RootElementHttpMessageConverter());
        return converters;
    }


    // посылаем запрос на JSON, XML десериализовать не получается
    @Test
    @Disabled
    public void givenConsumingJson_whenReadingTheReferenceDTO_thenCorrect() {
        String url = invUtil.getServerUrlPrefi() + "/references/{reference-id}";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setMessageConverters(getMessageConverters());
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<ReferenceDto> response =
                restTemplate.exchange(url, HttpMethod.GET, entity, ReferenceDto.class, "5");
        ReferenceDto resource = response.getBody();
        int i=1;
        assertThat(resource, notNullValue());
    }
    // конвертор добавлен в метод предыдущего теста
    private List<HttpMessageConverter<?>> getMessageConvertersJSON() {
        List<HttpMessageConverter<?>> converters =
                new ArrayList<HttpMessageConverter<?>>();
//        converters.add(new MappingJackson2HttpMessageConverter());

        return converters;
    }

    // со стандартными конверторами доходит только String в виде JSON
    // с конверторами в методе - либо 400 - BadRequest, либо нет подходящего конвертора
    @Test
    @Disabled
    public void givenConsumingXml_whenWritingTheReferenceDto_thenCorrect() {
        String url = invUtil.getServerUrlPrefi() + "/references";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setMessageConverters(getMessageConverters());
        class TestForm {
            String url = "https://google.com";
            Integer freePeriod = 4;
        }
        TestForm resource = new TestForm();
//        String resource = new String("{\"url\": \"http://google.com\", \"freePeriod\": \"4\"}");
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType((MediaType.APPLICATION_JSON));

        HttpEntity<TestForm > entity = new HttpEntity<>(resource,headers);
        int i=1;
        ResponseEntity<ReferenceDto> response = restTemplate.exchange(
                url, HttpMethod.POST, entity, ReferenceDto.class);
        ReferenceDto referenceDtoResponse = response.getBody();
        assertEquals(resource.url, referenceDtoResponse.getRedirectReference());
    }



    // apache клиент
    // запрос без Accept - по умолчанию возврат в формате JSON
    @Test
    @Disabled
    public void
            givenRequestWithNoAcceptHeader_whenRequestIsExecuted_thenDefaultResponseContentTypeIsJson()
            throws ClientProtocolException, IOException {
        // Given
        String jsonMimeType = "application/prs.hal-forms+json";
        String url = invUtil.getServerUrlPrefi() + "/references";
        HttpUriRequest request = new HttpGet( url);
        // When
        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        // Then
        String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
        assertEquals( jsonMimeType, mimeType );
    }


}

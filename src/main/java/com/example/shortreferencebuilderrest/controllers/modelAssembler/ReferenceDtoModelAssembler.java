package com.example.shortreferencebuilderrest.controllers.modelAssembler;

import com.example.shortreferencebuilderrest.controllers.ReferenceApiController;
import com.example.shortreferencebuilderrest.controllers.RequestApiController;
import com.example.shortreferencebuilderrest.forms.ReferenceForm;
import com.example.shortreferencebuilderrest.transfer.ReferenceDto;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.methodOn;

/**
 * Date: 13.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * Implements HyperLinks to the self, to the list of {@code references} and to the list of {@code requests}.
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Setter
@Getter
@Component
public class ReferenceDtoModelAssembler implements RepresentationModelAssembler<ReferenceDto, EntityModel<ReferenceDto>> {

    private Pageable pageable;
    private ReferenceForm referenceForm;

    @Override
    public EntityModel<ReferenceDto> toModel(ReferenceDto referenceDto) {
        return EntityModel.of(referenceDto,
                linkTo(methodOn(RequestApiController.class)
                        .getAllRequests(referenceDto.getId(), pageable))
                        .withRel("requests"),
                linkTo(methodOn(ReferenceApiController.class)
                        .getReferenceById(referenceDto.getId()))
                        .withSelfRel(),
// ссылки на методы
//                        .andAffordance(
//                                afford(methodOn(ReferenceApiController.class)
//                                        .changeState(referenceDto.getId())))
//                        .andAffordance(
//                                afford(methodOn(ReferenceApiController.class)
//                                        .deleteReference(referenceDto.getId())))
//                        .andAffordance(
//                                afford(methodOn(ReferenceApiController.class)
//                                        .createReference(referenceForm))),
                linkTo(methodOn(ReferenceApiController.class)
                        .getAllReferences(pageable))
                        .withRel("references"));

    }
}

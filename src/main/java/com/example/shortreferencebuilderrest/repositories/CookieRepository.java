package com.example.shortreferencebuilderrest.repositories;

import com.example.shortreferencebuilderrest.models.CookieInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Date: 13.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * Cookie's JPA repository.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Repository
public interface CookieRepository extends JpaRepository<CookieInfo, Long> {

    List<CookieInfo> findAllByRequestInfoIdOrderByIdAsc(Long requestInfo);
//    List<CookieInfo> findAllByRequestIdOrderByIdAsc(Long requestId);
}

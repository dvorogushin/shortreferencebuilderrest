package com.example.shortreferencebuilderrest.transfer;

import com.example.shortreferencebuilderrest.models.CookieInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.hateoas.server.core.Relation;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Date: 13.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * Converts the {@link CookieInfo cookie} entity to the {@link CookieInfoDto DTO} object.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Data
@AllArgsConstructor
@Builder
// Used for Hateoas descriptions
@Relation(collectionRelation = "cookies", itemRelation = "cookie")
public class CookieInfoDto {

    /**
     * Cookie's name.
     */
    private String name;

    /**
     * Cookie's value.
     */
    private String value;

    /**
     * Converts one {@link CookieInfo cookie} item.
     * @param cookieInfo Cookie's entity object.
     * @return {@link CookieInfoDto DTO} object.
     */
    public static CookieInfoDto from(CookieInfo cookieInfo) {
        return CookieInfoDto.builder()
                .name(cookieInfo.getName())
                .value(cookieInfo.getValue())
                .build();
    }

    /**
     * Converts collection of Cookies.
     * @param cookieInfos List of {@code Cookies}.
     * @return List of {@link CookieInfoDto DTO} objects.
     */
    public static List<CookieInfoDto> from(List<CookieInfo> cookieInfos) {
        return cookieInfos.stream().map(CookieInfoDto::from).collect(Collectors.toList());
    }

}

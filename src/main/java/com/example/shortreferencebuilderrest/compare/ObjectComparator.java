package com.example.shortreferencebuilderrest.compare;

import com.example.shortreferencebuilderrest.exceptions.NoSuchFieldExceptions;
import com.example.shortreferencebuilderrest.transfer.ReferenceDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Date: 02.04.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p>
 * Recursively compares objects according to the {@code PageRequest} (pageable).
 * Sorting order, properties and direction should be included in the {@code PageRequest} object.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
public class ObjectComparator<T> implements Comparator<T> {

    /**
     * PageRequest object.
     */
    private Pageable pageRequest;
    /**
     * Collection of sorting properties.
     */
    private List<Sort.Order> orders = new ArrayList<>();
    /**
     * Type of comparing objects.
     */
    private final Class<T> type;

    /**
     * Constructor
     *
     * @param pageRequest
     */
    public ObjectComparator(Pageable pageRequest, Class<T> type) {

        this.type = type;

        this.pageRequest = pageRequest;
        this.pageRequest.getSort()
                .stream()
                .forEach(or -> this.orders.add(or));
        orderIndex = 0;
        defineCurrentObjectField(orderIndex);
    }

    // The current data of the object
    Sort.Order order;
    Integer orderIndex;
    String property;
    Sort.Direction direction;
    Field field;
    Type fieldType;
    Integer result = 0;

    /**
     * Specifies the property should be compared.
     *
     * @param orderIndex The index of current the property.
     * @throws NoSuchFieldExceptions if field like property not found.
     */
    void defineCurrentObjectField(Integer orderIndex) {

        // current order
        order = orders.get(orderIndex);
        // current property
        property = order.getProperty();
        // current direction
        direction = order.getDirection();

        // get the comparable field of the object
        try {
            field = type.getDeclaredField(property);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            // TODO myException
            throw new NoSuchFieldExceptions();
        }
        field.setAccessible(true);

        // define the type of the field
        fieldType = field.getType();
    }


    /**
     * Compares values of object's fields according to their types.
     *
     * @param o1 extends {@link Object}
     * @param o2 extends {@link Object}
     * @return Integer {-1, 0, 1}
     */
    @Override
    public int compare(T o1, T o2) {

        // Compare values
        try {
            if (fieldType == Integer.class) result = ((Integer) field.get(o1)).compareTo((Integer) field.get(o2));
            if (fieldType == Long.class) result = ((Long) field.get(o1)).compareTo((Long) field.get(o2));
            if (fieldType == String.class) result = ((String) field.get(o1)).compareTo((String) field.get(o2));
            if (fieldType == LocalDateTime.class)
                result = ((LocalDateTime) field.get(o1)).compareTo((LocalDateTime) field.get(o2));
            if (fieldType == Boolean.class) result = ((Boolean) field.get(o1)).compareTo((Boolean) field.get(o2));
        } catch (IllegalAccessException | NullPointerException e) {
//            e.printStackTrace();
        }
        return multiSortingIfNecessary(result, o1, o2);
    }

    /**
     * Checks if sorting by the next property is required.
     * Comparing with the next property only works if:
     * <ul>
     * <li>the result of the previous comparison equals zero</li>
     * <li>the next property is present</li>
     * </ul>
     * Otherwise returns the previous result.
     *
     * @param previousResult - Integer
     * @param o1 extends {@link Object}
     * @param o2 extends {@link Object}
     * @return Integer {-1, 0, 1}
     */
    Integer multiSortingIfNecessary(Integer previousResult, T o1, T o2) {
        // if the values are equal, then check and compare the next order property (recursive)
        if (previousResult == 0 && orderIndex < orders.size() - 1) {
            orderIndex++;
            defineCurrentObjectField(orderIndex);
            return this.compare(o1, o2);
        }
        // define direction
        if (direction != Sort.Direction.ASC) {
            previousResult *= (-1);
        }
        // Specifies the properties to its initial state
        // prepare to compare next pairs of objects
        if (orderIndex > 0) {
            orderIndex = 0;
            defineCurrentObjectField(0);
        }
        return previousResult;
    }
}



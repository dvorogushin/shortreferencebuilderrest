package com.example.shortreferencebuilderrest.security.filter;

import com.example.shortreferencebuilderrest.forms.LoginCredentials;
import com.example.shortreferencebuilderrest.models.User;
import static com.example.shortreferencebuilderrest.security.config.SecurityConfiguration.*;
import com.example.shortreferencebuilderrest.security.details.UserDetailsImpl;
import com.example.shortreferencebuilderrest.security.model.Token;
import com.example.shortreferencebuilderrest.security.service.SessionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * Date: 25.04.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p> Filter for "/authorize" (login) endpoint.
 * Authenticates the user with a password credentials.
 * Creates client's session. Returns access and refresh tokens.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Slf4j
public class TokenAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private ObjectMapper objectMapper;

    private SessionService sessionService;

    public TokenAuthenticationFilter(AuthenticationManager authenticationManager, SessionService sessionService) {
        super(authenticationManager);
        this.sessionService = sessionService;
    }

    /**
     * Authenticates the {@code user} with a password credentials.
     *
     * @param request
     * @param response
     * @return
     * @throws AuthenticationException
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {


        String acceptHeader = request.getHeader(HttpHeaders.ACCEPT);
        if (acceptHeader != null && acceptHeader.equals(MediaType.APPLICATION_XML.toString())) {
            objectMapper = new XmlMapper();
            response.setContentType(MediaType.APPLICATION_XML.toString());
        } else {
            objectMapper = new JsonMapper();
            response.setContentType(MediaType.APPLICATION_JSON.toString());
        }

        try {
            log.info("Attempt authentication.");
            LoginCredentials credentials = objectMapper.readValue(request.getReader(), LoginCredentials.class);
            log.info("Authenticated by email {}", credentials.getEmail());
            UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(credentials.getEmail(), credentials.getPassword());

            return super.getAuthenticationManager().authenticate(token);
        } catch (IOException e) {
            throw new BadCredentialsException("Can't authenticate.");
        }
    }

    /**
     * Creates client's session. Returns access and refresh tokens.
     *
     * @param request
     * @param response
     * @param chain
     * @param authentication
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void successfulAuthentication(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain,
            Authentication authentication) throws IOException, ServletException {

        log.info("successAuthenticationMethod() is invoking.");
        Map<String, String> responseMap = new HashMap<>();

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        User user = userDetails.getUser();
        log.info("Authenticated by: " + user.getEmail());
        // create client session, get tokens
        Set<Token> tokens = sessionService.createSession(user);

        // insert access token to the response
        for (Token token : tokens) {
            if (token.getType().equals(Token.Type.ACCESS)) {
                responseMap.put(ACCESS_TOKEN, token.getEncryptedValue());
                responseMap.put(TOKEN_TYPE, TOKEN_TYPE_BEARER);
                responseMap.put(
                        EXPIRE_IN,
                        String.valueOf(
                                ChronoUnit.SECONDS.between(
                                        LocalDateTime.now(),
                                        LocalDateTime.ofInstant(
                                                Instant.ofEpochSecond(token.getExpirationTime()),
                                                TimeZone.getDefault().toZoneId()))));
            }
        }
        // insert refresh token to the response
        for (Token token : tokens) {
            if (token.getType().equals(Token.Type.REFRESH) && responseMap.containsKey(ACCESS_TOKEN)) {
                responseMap.put(REFRESH_TOKEN, token.getEncryptedValue());
            }
        }

        // OpenID Connect Core 1.0 (3.1.3.3.)
        response.setHeader(CACHE_CONTROL, CACHE_CONTROL_NO_STORE);
        response.setHeader(PRAGMA, PRAGMA_NO_CACHE);
        // send the tokens
        objectMapper.writeValue(response.getWriter(), responseMap);
//        objectMapper.writeValue(response.getWriter(), Collections.singletonMap(TOKEN, tokens));
    }
}
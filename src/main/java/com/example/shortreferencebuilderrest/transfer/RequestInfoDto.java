package com.example.shortreferencebuilderrest.transfer;

import com.example.shortreferencebuilderrest.models.RequestInfo;
import com.example.shortreferencebuilderrest.models.CookieInfo;
import com.example.shortreferencebuilderrest.services.CookieService;
import com.example.shortreferencebuilderrest.services.RequestService;
import com.example.shortreferencebuilderrest.services.UserService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.core.Relation;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Date: 13.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * Class converts {@link RequestInfo request} entity to {@link RequestInfoDto DTO} object,
 * which is a representational object for servlet response.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Data
@AllArgsConstructor
@Builder
// Used for Hateoas descriptions
@Relation(collectionRelation = "requests", itemRelation = "request")
@Component
public class RequestInfoDto {

    /**
     * {@code CookieInfo} service as static field to use in static methods.
     */
    private static CookieService cookieService;

    /**
     * Service injection to the static fields.
     *
     * @param cookieService
     */
    @Autowired
    private RequestInfoDto(CookieService cookieService) {
        RequestInfoDto.cookieService = cookieService;
    }

    /**
     * {@code Request's} ID.
     */
    private Long id;

    /**
     * {@code Request's} date and time of creation.
     */
    private LocalDateTime creationDate;

    /**
     * A {@code User-Agent} header value.
     */
    private String userAgent;

    /**
     * A Requester's IP address.
     */
    private String address;

    /**
     * A Requester's host port.
     */
    private String port;

    /**
     * Result of redirection.
     */
    private Boolean wasRedirected;

    /**
     * Quantity of {@link CookieInfo cookies} in total.
     */
    private Integer cookieCount;

    /**
     * Geographic country, which {@code request's} IP belongs to.
     */
    private String country;

    /**
     * Geographic city, which {@code request's} IP belongs to.
     */
    private String city;

    /**
     * Temperature in degrees at the moment of {@code request}.
     */
    private Integer temperature;

    /**
     * Collection of request's cookies.
     */
    private List<CookieInfoDto> cookies;

    /**
     * Reference's ID.
     */
    private Long referenceId;

    /**
     * Converts one {@link RequestInfo} object to {@code RequestInfoDto} object.
     *
     * @param requestInfo {@link RequestInfo} object.
     * @return {@code RequestInfoDto} object.
     */
    public static RequestInfoDto from(RequestInfo requestInfo) {

        // particular request to db because of FETCH.LAZY
        if (requestInfo.getCookies() !=null) {
            requestInfo.setCookies(cookieService.getCookieInfo(requestInfo.getId()));
        }

        return RequestInfoDto.builder()
                .id(requestInfo.getId())
                .creationDate(requestInfo.getCreationDateTime())
                .userAgent(requestInfo.getUserAgent())
                .address(requestInfo.getAddress())
                .port(String.valueOf(requestInfo.getPort()))
                .wasRedirected(requestInfo.getRedirected())
                .city(requestInfo.getCity())
                .country(requestInfo.getCountry())
                .temperature(requestInfo.getTemperature())
                .referenceId(requestInfo.getReference().getId())

                // FETCH.LAZY relation
                .cookieCount(requestInfo.getCookies().size())
                .cookies(CookieInfoDto.from(requestInfo.getCookies()))
                .build();
    }

    /**
     * Converts list of {@link RequestInfo} objects to list of {@code RequestInfoDto} objects.
     *
     * @param requestInfos List of {@link RequestInfo} objects.
     * @return List of {@code RequestInfoDto} objects.
     */
    public static List<RequestInfoDto> from(List<RequestInfo> requestInfos) {
        return requestInfos.stream().map(RequestInfoDto::from).collect(Collectors.toList());
    }
}

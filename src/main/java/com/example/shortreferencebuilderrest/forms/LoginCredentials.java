package com.example.shortreferencebuilderrest.forms;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Date: 25.04.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * Is used to map data during {@code user's} authorisation.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginCredentials implements Serializable {

    /**
     * User's email.
     */
    @NotNull
    @Length(min = 5, max = 30)
    private String email;

    /**
     * User's password.
     */
    @NotNull
    private String password;

}

package com.example.shortreferencebuilderrest.security.filter;

import com.example.shortreferencebuilderrest.models.User;

import static com.example.shortreferencebuilderrest.security.config.SecurityConfiguration.*;

import com.example.shortreferencebuilderrest.security.details.UserDetailsImpl;
import com.example.shortreferencebuilderrest.security.model.Token;
import com.example.shortreferencebuilderrest.security.service.SessionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.springframework.util.ObjectUtils.isEmpty;

/**
 * Date: 28.04.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p> Authenticates all requests, except requests to "/refresh" and "/authorize" endpoints.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Slf4j
@WebFilter("/*")
public class TokenAuthorizationFilter extends OncePerRequestFilter {

    private SessionService sessionService;

    public TokenAuthorizationFilter(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    /**
     * Checks the header, finds an access token and validates it.
     *
     * @param request
     * @param response
     * @param filterChain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (!isEmpty(authHeader) && authHeader.startsWith(TOKEN_TYPE_BEARER)) {
            // try decrypt and validate the token
            try {
                String encryptedToken = authHeader.substring(TOKEN_TYPE_BEARER.length() + 1);
                Token token = sessionService.getDecryptedToken(encryptedToken);
                if (token.getType().equals(Token.Type.ACCESS)) {
                    User user = token.getUser();
                    // set authentication
                    UserDetails userDetails = new UserDetailsImpl(user);
                    UsernamePasswordAuthenticationToken authenticationToken =
                            new UsernamePasswordAuthenticationToken(user.getEmail(), null, userDetails.getAuthorities());
                    SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                }
                // in ane case forward the request further
            } catch (Exception exc) {
            }
        }
        filterChain.doFilter(request, response);
    }
}
package com.example.shortreferencebuilderrest.security.service;

import com.example.shortreferencebuilderrest.models.User;
import com.example.shortreferencebuilderrest.security.model.Token;
import com.example.shortreferencebuilderrest.security.model.Session;

import java.util.List;
import java.util.Set;

/**
 * Date: 29.04.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p>
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */

public interface SessionService {

    Set<Token> createSession(User user);

    Session getSession(Long sessionId);

    List<Session> getAll();

    Token getDecryptedToken(String encryptedToken);

    Set<Token> refreshTokens(Token token);
}

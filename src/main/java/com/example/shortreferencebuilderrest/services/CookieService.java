package com.example.shortreferencebuilderrest.services;

import com.example.shortreferencebuilderrest.models.CookieInfo;

import java.util.List;

/**
 * Date: 05.05.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p>
 * Service interface for operations with {@code CookieInfo}.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */

public interface CookieService {
    List<CookieInfo> getCookieInfo(Long requestId);
}

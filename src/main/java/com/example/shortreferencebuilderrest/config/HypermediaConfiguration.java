package com.example.shortreferencebuilderrest.config;

import org.springframework.context.annotation.Configuration;

/**
 * Date: 15.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p>Used to add a hyperlinks to the methods of controllers.
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */

@Configuration
//@EnableHypermediaSupport(type = HAL_FORMS)
public class HypermediaConfiguration {
}

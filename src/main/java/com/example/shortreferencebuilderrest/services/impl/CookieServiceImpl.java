package com.example.shortreferencebuilderrest.services.impl;

import com.example.shortreferencebuilderrest.models.CookieInfo;
import com.example.shortreferencebuilderrest.repositories.CookieRepository;
import com.example.shortreferencebuilderrest.services.CookieService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Date: 05.05.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p>
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Service
@RequiredArgsConstructor
public class CookieServiceImpl implements CookieService {

    public final CookieRepository cookieRepository;

    @Override
    public List<CookieInfo> getCookieInfo(Long requestId) {
        return cookieRepository.findAllByRequestInfoIdOrderByIdAsc(requestId);
    }
}
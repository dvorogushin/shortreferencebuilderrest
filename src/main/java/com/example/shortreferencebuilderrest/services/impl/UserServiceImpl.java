package com.example.shortreferencebuilderrest.services.impl;

import com.example.shortreferencebuilderrest.exceptions.UserNotFoundException;
import com.example.shortreferencebuilderrest.models.User;
import com.example.shortreferencebuilderrest.repositories.UserRepository;
import com.example.shortreferencebuilderrest.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Date: 05.05.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p>
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public User getUser(Long userId) {

        User user = userRepository
                .findById(userId)
                .orElseThrow(UserNotFoundException::new);

        return user;
    }
}
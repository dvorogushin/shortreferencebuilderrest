package com.example.shortreferencebuilderrest.security.model;

import lombok.*;

import javax.persistence.*;
import java.nio.ByteBuffer;
import java.util.UUID;

/**
 * Date: 28.04.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilderrest">shortreferencebuilderrest</a></p>
 * <p> Client session entity. <br>
 * Consists of two random UUID keys.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Setter
@Getter
@AllArgsConstructor
@Entity
@Table(name = "IN_MEMORY_JWK")
public class Session {

    public Session() {
        this.encValue = UUID.randomUUID();
        this.sigValue = UUID.randomUUID();
    }

    /**
     * ID of the session.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /**
     * Encrypting key.
     */
    @Column(name = "enc")
    private UUID encValue;

    /**
     * Signature key.
     */
    @Column(name = "sig")
    private UUID sigValue;

    /**
     * Converts array of bytes to UUID format.
     *
     * @param bytes
     * @return
     */
    public static UUID asUuid(byte[] bytes) {
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        long firstLong = bb.getLong();
        long secondLong = bb.getLong();
        return new UUID(firstLong, secondLong);
    }

    /**
     * Converts UUID to array of bytes.
     *
     * @param uuid
     * @return
     */
    public static byte[] asBytes(UUID uuid) {
        ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.putLong(uuid.getMostSignificantBits());
        bb.putLong(uuid.getLeastSignificantBits());
        return bb.array();
    }
}